# XGBoost classification model based on NDMI & LAI

## Usage

```
python3 -m venv venv
pip install -r requirements.txt
source venv/bin/activate
python xgboost_mode.py # train the model
python xgboost_predict.py # inference graph for predictions on rasters of interest
```

## Requirements

Python 3.6+\
requirements.txt
