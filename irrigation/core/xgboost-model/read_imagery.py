# This is a script for extraction of pixel values overlapping training set into .csv file.
# .csv file is used in training the machine learning algorithm. 
# Don't change anything unless it states it can be changed ("YOU CAN CHANGE THIS").


import rasterio
import fiona
from rasterstats.io import Raster
import os
from datetime import datetime
import math

pt = datetime.now()
print()
print("Start time: "+str(pt))
print()

# YOU CAN CHANGE THIS
# create/open .csv file to store pixel values
outfile=open("../../data/models_csv/test_model.csv", 'w')

# YOU CAN CHANGE THIS
# training set
with fiona.open("../../data/training_set/area_34.shp", "r") as shapefile:
    geom_bounds=shapefile.bounds
    geom = [feature for feature in shapefile]

folder_c=0

with rasterio.open ("../../data/satellite_imagery/area_34_ndmi_lai.tiff") as raster_object:
    band_num=len(raster_object.read())
    print (band_num)

band_1=[]
band_2=[]
training_classes=[]

# overlap raster data with training set and write to .csv file
for folder in os.listdir("../../data/satellite_imagery/ndmi_lai"):
    print ("{} folders processed".format(str(folder_c)))
    folder_c+=1
    for item in os.listdir("../../data/satellite_imagery/ndmi_lai/"+folder):
        if item.endswith("merged.tiff"):
            for b in range (1, band_num+1):
                with Raster("../../data/satellite_imagery/ndmi_lai/"+folder+"/"+item, band=b) as raster_obj:
                    band_values=[]
                    raster_subset = raster_obj.read(bounds=geom_bounds) 
                    for i in geom:
                        polygon_mask = rasterio.features.geometry_mask(geometries=[i["geometry"]], out_shape=(raster_subset.shape[0], raster_subset.shape[1]), 
                                                                        transform=raster_subset.affine, all_touched=True, invert=True)
                        for j in raster_subset.array[polygon_mask]: 
                            if math.isnan(j)==False and j!=0 and b==1:
                                band_1.append(j)
                                training_classes.append(i["properties"]["Class"])
                            elif math.isnan(j)==False and j!=0 and b==2:
                                band_2.append(j)
                                training_classes.append(i["properties"]["Class"])
                
    
 
for i in range(len(band_1)):
    outfile.write('%s;%s;%s\n' %  (training_classes[i], band_1[i], band_2[i] ))

t = datetime.now()
print()
print("End time: "+str(t))
tt = t-pt
print("Duration: "+str(tt))
          