from joblib import load
import numpy as np
from numpy import loadtxt
import joblib
import rasterio
import rasterio.plot
from shapely.geometry import shape, Point, Polygon
import os


# Path to model file
model_file='../../data/models/test_model.joblib'

# Load xgboost model
xgboost_model = load(model_file)

# Path to raster file for prediction 
raster_file='../../data/satellite_data/storage/area_34_2020-06-29/indices/ndmi_lai.tif'

# Open raster file for prediction
bands=[]
with rasterio.open(raster_file) as src:
	data_ras = src.read()
	data_meta = src.profile

	band1 = src.read(1)
	band2 = src.read(2)
	height,width = band1.shape
	print (height)
	print (width)
	for h in range(height):
		for w in range(width):
			pixcoords = src.transform * (w,h)
			pixPoint = Point(pixcoords)
			print (pixPoint)
			v1 = band1[h,w]
			v2 = band2[h,w]
			bands.append(list([v1, v2]))

# extract metadata for the prediction and writing of the predicted raster
data_transform = data_meta["transform"]
data_crs = data_meta["crs"]
data_transform, data_crs
data_meta['dtype'] = "float64"

# X to array
X = np.asarray(bands)
print(X[5:15])

#predict new tif 
pred = xgboost_model.predict(X)
print(pred[50:100])
print(len(pred))

# write predicted raster values
with rasterio.open('../../data/predictions/test_pred.tif', 'w', **data_meta) as dst:
	pred_new = np.asarray(pred)
	pred_aray = np.reshape(pred_new, band1.shape)
	dst.write(pred_aray, 1)
