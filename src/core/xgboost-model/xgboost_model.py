from numpy import loadtxt
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix
import numpy as np
import joblib
from joblib import dump
import os

# Path to dataset .csv file
csv_file='../../data/csv/test.csv'

# Open dataset with numpy
dataset = loadtxt(csv_file, delimiter=",")

#split data into X and y
X = dataset[:, 1:]
y = dataset[:, 0]

# X and y to numpy arrays
X_array = np.asarray(X)

# model parameters
seed = 3
test_size = 0.15 

# split data to train and test
X_train, X_test, y_train, y_test = train_test_split(X_array, y, test_size=test_size, random_state=seed)

# fit model / logistic regression
model = XGBClassifier(learning_rate=0.15, max_depth=15, objective='reg:logistic')
model.fit(X_train, y_train)

# make predictions for test data
y_pred = model.predict(X_test)
predictions = [value for value in y_pred]

# evaluate predictions and show accuracy
accuracy = accuracy_score(y_test, predictions)
print("Accuracy: %.2f%%" % (accuracy * 100.0))

# Show confusion matrix
cm = confusion_matrix(y_test , y_pred)
print(cm)

# Dump the model 
dump(model, '../../data/models/test_model.joblib')